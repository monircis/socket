var app = require('express')();
var http = require('http').createServer(app);
let  io = require('socket.io')(http);


app.get('/', (req, res) => {
    res.sendFile(__dirname+'/index.html');
});

http.listen(3000, () => {
    console.log('listening on *:3000');
});

io.on('connection',(socket)=>{


   console.log('Socket connected');
   socket.on('disconnect', ()=>{
    console.log('Disconnected');
   });

   socket.on('Created',(data)=>{
 //console.log( io.engine.clientsCount);
       socket.username = data;
       console.log(data);
       io.emit('users',Object.keys(io.eio.clients));
        //console.log( Object.keys(io.eio.clients) );
       socket.broadcast.emit('Created',(data));
   })
socket.on('chat-message',(data)=>{
    socket.broadcast.emit('chat-message',(data));
})
    socket.on('typing',(data)=>{
        socket.broadcast.emit('typing',(data));
    })
    socket.on('stopTyping',(data)=>{
        socket.broadcast.emit('stopTyping',(data));
    })
    socket.on('joined',(data)=>{

        socket.broadcast.emit('joined',(data));
    })
});